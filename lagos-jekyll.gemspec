# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "lagos-jekyll"
  spec.version       = "0.3.0"
  spec.authors       = ["xin_chao"]
  spec.email         = ["xinchao@gmail.com"]

  spec.summary       = %q{Forty by HTML5UP reloaded.}
  spec.homepage      = "https://bitbucket.org/xin_chao/lagos-jekyll"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_development_dependency "jekyll", "~> 3.3"
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end

